Team Name (Responsibilities):
- Suryanto Phienanda (uvm_scoreboards, uvm_monitors, uvm_environment, uvm_test, uvm_top, uvm_agent)
- Aishwarya Mahesh (dut, uvm_interface)
- Sagar Oza (uvm_seq_item, uvm_sequences, uvm_driver, uvm_sequencer)

This project is to verify SPI transaction from the APB level using Universal Verification Methodology (UVM) on LPC20xx.

To perform this project, the team relied on the LPC20xx manual to understand how the transaction works from APB - SPI bus.

Each member of the team has responsibilities that will eventually be combined to verify the transaction.

To proceed, the team must first understand what each pin does on APB and SPI. Since my responsibilities was on the SPI bus, therefore, i focused on the SPI pins. 
In SPI, there are MOSI, MISO, CS and SCLK pins.
- SCLK: This pin is used to obtain the clock for the SPI bus. Typically this pin performs in the MHz range.
- MOSI: This pin is used to transfer data from master to slave (write). The data is transferred one bit at a time.
- MISO: This pin is used to transfer data from slave to master (read). The data is transferred one bit at a time.
- CS: This pin is used to talk to the slave. It is active-low, therefore, only when this pin is low (asserted) then the device can talk to each other.

To perform this verification, the approach that i used was to have reference model scoreboards to compare the results from the predictor scoreboards (read/write) with the values in the data registers. The predictor scoreboards will receive one data at a time until the number of BITS is satisfied.
The number of BITS is set before the transaction by setting the BITS register inside LPC20xx. Based on this register, the scoreboards will determine how many bits to compare with the reference model.
