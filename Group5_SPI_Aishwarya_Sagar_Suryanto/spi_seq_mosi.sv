class spi_seq_mosi extends uvm_sequence #(spi_item_mosi);
	`uvm_object_utils(spi_seq_mosi)
	
	spi_item_mosi seq_itm;
	
	function new(string name = "spi_seq_mosi");
		super.new(name);
	endfunction

	//seq_itm.spi_cr cr;
	//seq_itm.spi_ccr ccr;

	task body();
	
		
		seq_itm = spi_item_mosi::type_id::create("seq_itm");
		`uvm_info("MOSI seq:: ", "Start the Sequence", UVM_MEDIUM)

		begin
			
					start_item(seq_itm);
					
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 0; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 0; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 0; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 1; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 0; seq_itm.cr.CPOL == 1; seq_itm.cr.CPHA == 0; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 0; seq_itm.cr.CPOL == 1; seq_itm.cr.CPHA == 1; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 1; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 0; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 1; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 1; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin						
					start_item(seq_itm);
					
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 1; seq_itm.cr.CPOL == 1; seq_itm.cr.CPHA == 0; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 1; seq_itm.cr.CPOL == 1; seq_itm.cr.CPHA == 1; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		begin
			
					start_item(seq_itm);
					
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 0; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 0; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 0; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 1; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 0; seq_itm.cr.CPOL == 1; seq_itm.cr.CPHA == 0; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 0; seq_itm.cr.CPOL == 1; seq_itm.cr.CPHA == 1; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 1; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 0; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 1; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 1; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end
		begin						
					start_item(seq_itm);
					
					seq_itm.randomize() with {seq_itm.pwrite == 1'b1;  seq_itm.ccr.clk_DIV == 8; seq_itm.cr.LSBF == 1; seq_itm.cr.CPOL == 1; seq_itm.cr.CPHA == 0; seq_itm.cr.bit_en == 0; seq_itm.cr.BITS == 4'b1000;};
					
					seq_itm.disp();	
					
					finish_item(seq_itm);
		end

		
		end
	endtask : body

endclass : spi_seq_mosi
