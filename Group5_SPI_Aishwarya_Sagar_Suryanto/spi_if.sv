interface spi_if(input reg pclk, input reg prst_n);

	//logic pclk;
	//logic prst_n;
	logic [31:0] paddr;
	logic pwrite;
	logic psel;
	logic penable;
	logic [31:0] pwdata;
	logic [31:0] prdata;
	logic pready;
	logic sclk; // op
	logic miso; // op
	logic ss; // op
	logic mosi; // op
	
	typedef struct packed{
		logic bit_en;
		logic [1:0] reserved;
		logic MSTR;
		logic LSBF;
		logic SPIE;
		logic [3:0] BITS;
		logic CPOL;
		logic CPHA;
		logic [15:12] rsvd;
	}spi_cr;
	
	typedef struct packed{
		logic[7:0] clk_DIV;
	}spi_ccr;
	
	typedef struct packed{
		logic SPIF;
		logic WCOL;
		logic ROVR;
		logic MODF;
		logic ABRT;
		logic[2:0] rsvd;
	}spi_sr;

	spi_cr cr;
	spi_ccr ccr;
	spi_sr sr;

	modport spi(input pclk, input prst_n, input paddr, input pwrite, input pwdata, input psel, input penable, output prdata, output pready, input miso, output ss, output mosi, output sclk, input cr, input ccr, output sr);	
endinterface
