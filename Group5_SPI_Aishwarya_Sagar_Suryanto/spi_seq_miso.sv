class spi_seq_miso extends uvm_sequence #(spi_item_miso);
	`uvm_object_utils(spi_seq_miso)
	
	spi_item_miso seq_itm;
	
	function new(string name = "spi_seq_miso");
		super.new(name);
	endfunction

	task body();

		seq_itm = spi_item_miso::type_id::create("seq_itm");
		`uvm_info(get_type_name(), "sequence start", UVM_MEDIUM)
		
		begin
			for(int i = 0; i<2; i=i+1)
				begin
					start_item(seq_itm);
					seq_itm.randomize() with {seq_itm.pwrite == 1'b0; seq_itm.cr.LSBF == i; seq_itm.cr.CPOL == 0; seq_itm.cr.CPHA == 0; seq_itm.ccr.clk_DIV == 8; seq_itm.cr.bit_en == 0; };
					//seq_itm.disp();
					finish_item(seq_itm);
				end
		end
	endtask : body
endclass : spi_seq_miso



