class spi_agent extends uvm_agent;
	`uvm_component_utils(spi_agent)

	spi_miso_driver miso_drv;
	spi_mosi_driver mosi_drv;
	spi_mosi_sequencer mosi_seqr;
	spi_miso_sequencer miso_seqr;
	/*ref_model_wr_mon wr_ref_mon;
	ref_model_rd_mon rd_ref_mon;*/
	virtual spi_if intf;

	function new(string name = "spi_agent", uvm_component parent=null);
		super.new(name,parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		miso_drv = spi_miso_driver::type_id::create("miso_drv",this);
		mosi_drv = spi_mosi_driver::type_id::create("mosi_drv",this);
		miso_seqr = spi_miso_sequencer::type_id::create("miso_seqr",this);
		mosi_seqr = spi_mosi_sequencer::type_id::create("mosi_seqr",this);
		if(!uvm_config_db#(virtual spi_if)::get(this,"*","intf", intf)) begin
			`uvm_fatal("SPI AGENT", "Failed in getting the interface!!!")
		end
	endfunction
	
	function void connect_phase(uvm_phase phase);
		miso_drv.seq_item_port.connect(miso_seqr.seq_item_export);
		mosi_drv.seq_item_port.connect(mosi_seqr.seq_item_export);
	endfunction

endclass
