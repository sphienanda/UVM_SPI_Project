class ref_model_rd_sb extends uvm_scoreboard;
	`uvm_component_utils(ref_model_rd_sb)
	
	// data_wr maybe [31:0] ??
	uvm_tlm_analysis_fifo#(spi_item_miso) data_from_ref_mod_mon_port;
	
	uvm_analysis_port #(logic[31:0]) prdata_port; //not sure if this works
	uvm_analysis_port #(spi_item_miso) miso_reg_value; // this packet contians CPOL, CPHA, LSBF and bit_en 
	
	spi_item_miso itm;
	spi_item_miso reg_itm;
	
	function new(string name="ref_model_rd_sb", uvm_component parent=null);
		super.new(name,parent);
			data_from_ref_mod_mon_port = new("data_from_ref_mod_mon_port", this);
			prdata_port = new("prdata_port", this);
			miso_reg_value = new("miso_reg_value",this);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
	endfunction
	
	int pclk, pclk_period, sclk, sclk_period;
	logic[31:0] prdata = 0;
	logic[3:0] nbits;
	
	task run_phase(uvm_phase phase);
		forever begin
			data_from_ref_mod_mon_port.get(itm);
			
			reg_itm = new();
			
			nbits = itm.cr.BITS;
			prdata = itm.prdata;
			
			prdata_port.write(prdata);
			
			reg_itm.cr.CPOL = itm.cr.CPOL; 
			reg_itm.cr.CPHA = itm.cr.CPHA;
			reg_itm.cr.bit_en = itm.cr.bit_en;
			reg_itm.cr.LSBF = itm.cr.LSBF;
			reg_itm.pwrite = itm.pwrite;
			
			miso_reg_value.write(reg_itm); // MODIFIED it was reg_itm.write(miso_reg_value)
		end
	endtask

endclass
