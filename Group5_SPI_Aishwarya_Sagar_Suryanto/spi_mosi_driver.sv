class spi_mosi_driver extends uvm_driver #(spi_item_mosi);
	`uvm_component_utils(spi_mosi_driver);
	
	virtual spi_if v_if;
	spi_item_mosi seq_itm;
	reg[1:0] State = 0;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new
	
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		seq_itm = spi_item_mosi::type_id::create("seq_itm_driver");
		if (!uvm_config_db #(virtual spi_if)::get(this, "", "intf", v_if))
			begin
			`uvm_error("FATAL MSG", "Object configuration failed");
			end
		$display("Driver - Build Phase");
		$display("[DRIVER]: Build Phase started");
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
	endfunction: connect_phase

	task run_phase(uvm_phase phase);
		// Body for Run phase
		forever begin
			@(posedge v_if.pclk);
			//$display(State);
			case(State) 
				//STATE 0
				0: begin
					seq_item_port.get_next_item(seq_itm);// State0
					v_if.psel <= 1;
					v_if.paddr <= seq_itm.paddr;	
					v_if.pwrite <= seq_itm.pwrite;
					v_if.pwdata <= seq_itm.pwdata; //state 0
					v_if.cr <= seq_itm.cr;
					v_if.ccr <= seq_itm.ccr;
					//v_if.sr <= seq_itm.sr;
					State<=1;
				end
				
				//STATE 1
				1:
					begin
						v_if.penable <=1;		//State1
						State<=2;
					end
				//STATE 2
				2:	
					begin
					if(v_if.pready == 1)begin //state 2
					seq_item_port.item_done();
					v_if.penable <=0;
					State <= 0;
					end else State<=2;
				end

			endcase
		
			//seq_item_port.item_done();
		end
	endtask: run_phase

endclass: spi_mosi_driver

