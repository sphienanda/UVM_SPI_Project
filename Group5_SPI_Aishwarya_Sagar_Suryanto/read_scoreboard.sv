class spi_read_sb extends uvm_scoreboard;
	`uvm_component_utils(spi_read_sb)
	
	uvm_tlm_analysis_fifo#(logic[31:0]) data_port;
	uvm_tlm_analysis_fifo#(logic[31:0]) data_ref_port;
	
	function new(string name="spi_read_sb", uvm_component parent=null);
		super.new(name,parent);
	endfunction
	
	function void build_phase(uvm_phase phase);
		data_port = new("data_port",this);
		data_ref_port = new("data_ref_port",this);
	endfunction
	
	logic[31:0] data_from_mon;
	logic[31:0] data_from_ref;
	
	task run_phase(uvm_phase phase);
		forever begin
			data_port.get(data_from_mon);
			data_ref_port.get(data_from_ref);
			if(data_from_mon == data_from_ref) begin
				`uvm_info(get_type_name(), " Read function passed!", UVM_MEDIUM)
			end else begin
				`uvm_error(get_type_name(), " Data mismatched!")
			end
		end
	endtask

	/*
		spi_mode_from_rd_mon_port.get(spi_mode);
		bits_from_rd_mon_port.get(BITS);
		lsbf_from_rd_mon_port.get(LSBF);
		bit_en_from_rd_mon_port.get(bit_en);
		data_from_ref_model_port.get(ref_model_data);
		pwrite_from_rd_mon_port.get(pwrite);
		sclk_from_rd_mon_port.get(sclk);
		ssel_from_rd_mon_port.get(ssel);
		miso_from_rd_mon_port.get(miso);
		
		forever begin
			if((spi_mode == 0) || (spi_mode == 3)) begin
				if(sclk == 1) begin
					if(ssel == 0) begin
						if(lsbf_flag == 0) begin
						//data_from_register = 16'h0000;
							if(LSBF == 1) begin
								counter = 0;
							end else if(LSBF == 0) begin
								if(BITS == 4'b1000) begin
									counter = 7;
								end else if((BITS > 4'b1000 || BITS == 4'b0000) && bit_en == 1) begin
									if(BITS > 4'b1000) begin
										counter = BITS - 1;
									end else begin
										counter = 15;
									end
								end else 
									`uvm_fatal("spi_read_sb","More than 8-bits transfer occurs when BitEnable is not set to 1")
							end
							lsbf_flag = 1;
						end
						
						if(bit_en == 0) begin
							if(LSBF == 1) begin
								data_from_register[counter] = miso; // Counter starts from 0
								counter = counter + 1;
								if(counter > 7) begin
									if(pwrite == 0) begin
										if(data_from_register[7:0] == ref_model_data[7:0]) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end
							end else if(LSBF == 0) begin
								data_from_register[counter] = miso;
								counter = counter - 1;
								if(counter < 0) begin
									if(pwrite == 0) begin
										if(data_from_register[7:0] == ref_model_data[7:0]) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end
							end
						end else if(bit_en == 1) begin
							if(LSBF == 1) begin
								data_from_register[counter] = miso;
								counter = counter + 1;
								if((BITS != 4'b0000) && (counter > (BITS -1))) begin
									if(pwrite == 0) begin
										if((data_from_register[15:8] == ref_model_data[15:8]) && (data_from_register[7:0] == ref_model_data[7:0])) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end else if((BITS == 4'b0000) && (counter > 15)) begin
									if(pwrite == 0) begin
										if((data_from_register[15:8] == ref_model_data[15:8]) && (data_from_register[7:0] == ref_model_data[7:0])) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end
							end else if(LSBF == 0) begin
								data_from_register[counter] = miso;
								counter = counter - 1;
								if(counter < 0) begin
									if(pwrite == 0) begin
										if((data_from_register[15:8] == ref_model_data[15:8]) && (data_from_register[7:0] == ref_model_data[7:0])) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end
							end
						end
					end
				end
			end else if((spi_mode == 1) || (spi_mode == 2)) begin
				if(sclk == 0) begin
					if(ssel == 0) begin
						// create a miso flag by checking PWRITE from APB
						if(lsbf_flag == 0) begin
						//data_from_register = 16'h0000;
							if(LSBF == 1) begin
								counter = 0;
							end else if(LSBF == 0) begin
								if(BITS == 4'b1000) begin
									counter = 7;
								end else if((BITS > 4'b1000 || BITS == 4'b0000) && bit_en == 1) begin
									if(BITS > 4'b1000) begin
										counter = BITS - 1;
									end else begin
										counter = 15;
									end
								end else 
									`uvm_fatal("spi_read_sb","More than 8-bits transfer occurs when BitEnable is not set to 1")
							end
							lsbf_flag = 1;
						end
						
						if(bit_en == 0) begin
							if(LSBF == 1) begin
								data_from_register[counter] = miso; // Counter starts from 0
								counter = counter + 1;
								if(counter > 7) begin
									if(pwrite == 0) begin
										if(data_from_register[7:0] == ref_model_data[7:0]) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end
							end else if(LSBF == 0) begin
								data_from_register[counter] = miso;
								counter = counter - 1;
								if(counter < 0) begin
									if(pwrite == 0) begin
										if(data_from_register[7:0] == ref_model_data[7:0]) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end
							end
						end else if(bit_en == 1) begin
							if(LSBF == 1) begin
								data_from_register[counter] = miso;
								counter = counter + 1;
								if((BITS != 4'b0000) && (counter > (BITS -1))) begin
									if(pwrite == 0) begin
										if((data_from_register[15:8] == ref_model_data[15:8]) && (data_from_register[7:0] == ref_model_data[7:0])) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end else if((BITS == 4'b0000) && (counter > 15)) begin
									if(pwrite == 0) begin
										if((data_from_register[15:8] == ref_model_data[15:8]) && (data_from_register[7:0] == ref_model_data[7:0])) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end
							end else if(LSBF == 0) begin
								data_from_register[counter] = miso;
								counter = counter - 1;
								if(counter < 0) begin
									if(pwrite == 0) begin
										if((data_from_register[15:8] == ref_model_data[15:8]) && (data_from_register[7:0] == ref_model_data[7:0])) begin
											`uvm_info("spi_read_sb", "PASSED!! Data from miso same as Data Reg", UVM_MEDIUM)
										end else begin
											`uvm_error("spi_read_sb", "FAILED!! Data not the same as Data Reg")
										end
									end
									lsbf_flag = 0;
								end
							end
						end
					end
				end
			end
		end
	endtask
	*/
endclass

