class ref_model_wr_mon extends uvm_monitor;
	`uvm_component_utils(ref_model_wr_mon)
	
	uvm_analysis_port #(spi_item_mosi) wr_mon_port;
	
	virtual spi_if intf;
	spi_item_mosi itm;
	
	function new(string name="ref_model_wr_mon", uvm_component parent=null);
		super.new(name,parent);
		wr_mon_port = new("wr_mon_port", this);
	endfunction

	function void build_phase(uvm_phase phase);
		if(!uvm_config_db#(virtual spi_if)::get(this,"*","intf", intf)) begin
			`uvm_fatal("REF MODEL WRITE MONITOR", "Failed in getting the interface!!!")
		end
	endfunction

	virtual task run_phase(uvm_phase phase);
		begin
	`uvm_info("REF-WR MONITOR", "Inside sclk monitor run phase", UVM_MEDIUM)
			fork
				forever begin
					@(posedge intf.pclk);
						if(intf.pwrite == 1) begin
							itm = new();
							itm.ccr.clk_DIV = intf.ccr.clk_DIV;
							itm.pwrite = intf.pwrite;
							itm.pwdata = intf.pwdata;
							itm.cr.CPOL = intf.cr.CPOL; //we need to define these in the interface because we need to pass this also to DUT
							itm.cr.CPHA = intf.cr.CPHA;
							itm.cr.bit_en = intf.cr.bit_en;
							itm.cr.LSBF = intf.cr.LSBF;
							itm.cr.BITS = intf.cr.BITS;
							wr_mon_port.write(itm);							
					end
				
				end
			join_none
		end
	endtask : run_phase

endclass
