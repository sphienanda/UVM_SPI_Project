class spi_sck_sb extends uvm_scoreboard;
	`uvm_component_utils(spi_sck_sb)
	
	uvm_tlm_analysis_fifo#(int) counter_from_sck_mon; //sclk samples from monitor
	uvm_tlm_analysis_fifo#(logic) CPOL_from_sck_mon;
	uvm_tlm_analysis_fifo#(logic[7:0]) sck_freq_from_ref_mod;
	uvm_tlm_analysis_fifo#(spi_item_mosi) itm_from_ref_mod;
	
	spi_item_mosi mosi_itm;
	
	function new(string name="spi_sck_sb", uvm_component parent=null);
		super.new(name,parent);
	endfunction
	
	function void build_phase(uvm_phase phase);
		counter_from_sck_mon = new("counter_from_sck_mon",this);
		CPOL_from_sck_mon = new("CPOL_from_sck_mon",this);
		sck_freq_from_ref_mod = new("sck_freq_from_ref_mod",this);
		itm_from_ref_mod = new("itm_from_ref_mod",this);
	endfunction
	
	int counter;
	logic[7:0] sck_freq;
	logic CPOL;
	
	task run_phase(uvm_phase phase);
		forever begin
			mosi_itm = new();
			counter_from_sck_mon.get(counter);
			sck_freq_from_ref_mod.get(sck_freq);
			CPOL_from_sck_mon.get(CPOL);
			itm_from_ref_mod.get(mosi_itm);
			//checking CPOL
			if(counter == (sck_freq)) begin
				`uvm_info("SB_SCLK", "SCLK check is correct", UVM_MEDIUM)
			end
			else begin
				$display("ctr: %d sck: %d",counter, sck_freq);
				`uvm_error("SB_SCLK", "SCLK error")
			end
		end	

	endtask
	
endclass

