`timescale 1ns / 1ps

`define IDLE     2'b00
`define SETUP    2'b01
`define W_ENABLE 2'b10
`define R_ENABLE 2'b11
typedef struct packed {
		logic bit_en;
		logic [1:0] reserved;
		logic MSTR;
		logic LSBF;
		logic SPIE;
		logic [3:0] BITS;
		logic CPOL;
		logic CPHA;
		logic [15:12] rsvd;
	}spi_cr;
	
	typedef struct packed{
		logic[7:0] clk_DIV;
	}spi_ccr;
	
	typedef struct packed{
		logic SPIF;
		logic WCOL;
		logic ROVR;
		logic MODF;
		logic ABRT;
		logic[2:0] rsvd;
	}spi_sr;

	
module spi
  //#(
  // parameter CLK_DIV = 8
  //)
  (
   
   input        prst_n,     
   input        pclk,       
   input        psel,
   input        pwrite,
   input [31:0] pwdata,    
   input        penable,     
   output reg [31:0] prdata,   
   output reg pready,
   input [31:0] paddr,
   output sclk,
   input  miso,
   output mosi,
   output reg ss,
   input  spi_ccr ccr,
   input spi_cr cr,
   output spi_sr sr
   );
	//spi_cr cr;
	//spi_ccr ccr;
	//spi_sr sr;
  /*wire [4:0] nbits;
  wire [4:0] CLK_DIV;
  
  assign nbits = 8;
	assign CLK_DIV = 8;*/
reg mosi_start;
reg sclk_st_pulse;

  spi_master 
    //#(
    //  .CLK_DIV(CLK_DIV)
     // ) 
  U1
   (
   .prst_n(prst_n),     
   .pclk(pclk),         
   .pwdata(pwdata[15:0]),         
   .sclk_st_pulse(sclk_st_pulse),             
   .prdata(prdata),   
   .sclk(sclk),
   .miso(miso),
   .mosi(mosi),
   .mosi_start(mosi_start),
   .ss(ss),
   .cr(cr),
   .sr(sr),
   .ccr(ccr)
   );
   
reg [1:0] state;
reg [15:0] mem;



always @(posedge prst_n or posedge pclk) begin
//`uvm_info("DUTitem", $sformatf("PWRITE = %b, PWDATA = %x, CPOL = %b, CPHA = %b, CLK DIV = %b, LSBF = %b", pwrite, pwdata, cr.CPOL, cr.CPHA, ccr.clk_DIV,cr.LSBF), UVM_MEDIUM)
  if (prst_n == 0) begin
    state <= `IDLE;
    //prdata <= 0;
    pready <= 0;
    mem <= 0;
    end
  else begin
    case (state)
      `IDLE : begin
        //prdata <= 0;
         pready <= 0;
        if (psel) begin
            state <= `SETUP;
        end
        else begin
            state <= `IDLE;
        end
       end
       `SETUP : begin
            sclk_st_pulse <= 1'b1;
            @(posedge pclk)
            sclk_st_pulse <= 1'b0;
          if (penable && pwrite) begin
              // o_TX_Ready <= 1'b1;
               state <= `W_ENABLE;
		mosi_start <= 1'b1;
	  @(posedge pclk)
		@(posedge pclk)
            mosi_start <= 1'b0;
          end
          else if (penable && !pwrite) begin
            state <= `R_ENABLE;
          end
        end
      `W_ENABLE : begin
        if (psel && pwrite) begin
	mem <= pwdata;
	  
           //write to spi data register
          if(ss == 1) begin
            state <= `IDLE;
            pready <= 1'b1;          
          end
          else
           state <= `W_ENABLE;
      end
end
      `R_ENABLE : begin
        if (psel && !pwrite) begin
          //pready <= 1;
          //sclk_st_pulse <= 1'b0;
          //prdata <= mem[paddr]; //read from SPI data register
        if(ss == 1) begin
            state <= `IDLE;
            pready <= 1'b1;          
          end
          else
           state <= `R_ENABLE;
      end
      end
      default: begin
        state <= `IDLE;
      end
    endcase
  end
end

endmodule



module spi_master
 //#(
 // parameter CLK_DIV = 8)
  
   (input       prst_n,     
   input        pclk,       
   input [15:0] pwdata,
   input sclk_st_pulse,
   output reg [15:0] prdata,
input mosi_start,
   output reg sclk,
   input      miso,
   output reg mosi,
   output reg ss,
   input  spi_ccr ccr,
   input spi_cr cr,
   output spi_sr sr
  
   );
  
  //reg [3:0]nbits = SPCR.BITS;
  wire CPOL;     // Clock polarity
  wire CPHA;     // Clock phase
  //reg [$clog2(CLK_DIV*2)-1:0] counter_sclk;
  reg [31:0] counter_sclk;
  reg sclk_st_pulse, spi_clk, ledge, tedge;
  reg [5:0] counter_sclk_edge;
  //reg [nbits-1:0] shift_reg = 0;
  reg [15:0] shift_reg = 0;
  reg [3:0] miso_shift_bit;
  reg [3:0] mosi_shift_bit;
  reg [1:0] st = 0;
  reg [1:0] nxt_st = 0;
  
  assign CPOL = cr.CPOL;
  assign CPHA = cr.CPHA;
  
reg r_CS_n;
reg [4:0]nbits = 0;
reg [7:0] CLK_DIV;
    
always @(posedge pclk or negedge prst_n)
  begin
    CLK_DIV <= ccr.clk_DIV;
    if(cr.bit_en == 0)
	begin
	nbits = 8;
	end
    else if(cr.bit_en == 1) begin
	if(cr.BITS == 4'b000) begin
	nbits = 16;
	end
	else
	nbits = cr.BITS;
    end

    if (~prst_n)
    begin
      shift_reg <= 16'h0;
      
      counter_sclk_edge <= 0;
      ledge <= 1'b0;
      tedge <= 1'b0;
      spi_clk <= CPOL;
      sclk <= CPOL; 
      counter_sclk <= 0;
      r_CS_n  <= 1'b1;
    end
    else
    begin
	case(st)
		0:begin
        ledge  <= 1'b0;
		tedge <= 1'b0;
        if (sclk_st_pulse)
			begin
			 if (r_CS_n)
			 begin
			     r_CS_n <= 1'b0;
			 end
			shift_reg <= pwdata;
			
			counter_sclk_edge <= 2*nbits; //2*nbits // Total # edges depend on the no.of bits
			st <= 1;
			end
		
		end
		1: begin
		    ledge  <= 1'b0;
            tedge <= 1'b0;
        
			if (counter_sclk_edge > 0) //based on number of bits to be transmitted
				begin
				
				if (counter_sclk == CLK_DIV*2-1)
				begin
				counter_sclk_edge <= counter_sclk_edge - 1;
				tedge <= 1'b1;
				counter_sclk <= 0;
				spi_clk       <= ~spi_clk;
				//nxt_st <= 2;
				end
				else if (counter_sclk == CLK_DIV-1)
				begin
				  counter_sclk_edge <= counter_sclk_edge - 1;
				  ledge  <= 1'b1;
				  counter_sclk <= counter_sclk + 1;
				  spi_clk       <= ~spi_clk;
				end
				 
				else
				begin
				  counter_sclk <= counter_sclk + 1;
				end
			
		end  
      else
      begin
		st <= 2;
		 r_CS_n <= 1'b1;
		sr.SPIF <= 1'b1;
	end
	sclk <= spi_clk;
end
2: begin
   st <= 0;
	  end
  endcase
  end
  end
   
   
reg [2:0]st_data =0;

 
always @(posedge pclk or negedge prst_n)
  begin
    
    if (~prst_n)
    begin
      mosi     <= 1'b0;
      prdata      <= 8'h00;
      if(cr.LSBF == 0)
      begin
      mosi_shift_bit <= nbits; // send MSb first
      miso_shift_bit <= nbits;
      end
      else if(cr.LSBF == 1)
      begin
       mosi_shift_bit <= 0; // send LSb first
      miso_shift_bit <= 0;
      end
      
      
    end
    else
    begin
    case(st_data)
    0:begin
      //mosi_start <= sclk_st_pulse;// If ready is high, reset bit counts to default
      if (~sclk_st_pulse)
      begin
	if(cr.LSBF == 0)
      	begin
        mosi_shift_bit <= nbits-1;
        miso_shift_bit <= nbits-1;
	end
	else if(cr.LSBF == 1)
	begin
	mosi_shift_bit <= 0;
        miso_shift_bit <= 0;
	end
        st_data = 1;
      end
      
      end

      1:begin
	//mosi_start <= sclk_st_pulse;	
         if (mosi_start & ~CPHA)
          begin
	    if(cr.LSBF == 0)
	    begin
            mosi     <= shift_reg[nbits-1];
            mosi_shift_bit <= nbits-2;
	    end
	    else if(cr.LSBF == 1)
	    begin
	    mosi     <= shift_reg[0];
            mosi_shift_bit <= 1;
	    end
          end
         else if ((ledge & CPHA) | (tedge & ~CPHA))
          begin
            if(cr.LSBF == 0)begin
	    mosi_shift_bit <= mosi_shift_bit - 1;
            mosi     <= shift_reg[mosi_shift_bit];
	    end
	    else if(cr.LSBF ==1) begin
            mosi_shift_bit <= mosi_shift_bit + 1;
            mosi     <= shift_reg[mosi_shift_bit];
	    end
          end
          
	
	 if ((ledge & ~CPHA) | (tedge & CPHA))
            begin
	    if(cr.LSBF == 0)begin
            prdata[miso_shift_bit] <= miso;  // Sample data
            miso_shift_bit  <= miso_shift_bit - 1;
	    end
	    else if(cr.LSBF == 1)begin
	    prdata[miso_shift_bit] <= miso;  // Sample data
            miso_shift_bit  <= miso_shift_bit + 1;
	    end
            end
	
         if(cr.LSBF == 0)begin
          if(mosi_shift_bit == 0 && counter_sclk_edge == 0)
          begin
          st_data = 2;
          end
          else
          st_data = 1;
	end
	else if (cr.LSBF == 1) begin
	if(mosi_shift_bit == (nbits-1) && counter_sclk_edge == 0)
          begin
          st_data = 2;
          end
          else
          st_data = 1;
	end
     end

     2: begin
     st_data = 0;
     end
   endcase
      
    end
  end
 assign ss = r_CS_n; 
endmodule
