class ref_model_wr_sb extends uvm_scoreboard;
	`uvm_component_utils(ref_model_wr_sb)
	
	// data_wr maybe [31:0] ??
	uvm_tlm_analysis_fifo#(spi_item_mosi) data_from_ip_mon_port;
	
	uvm_analysis_port #(logic[7:0]) sclk_freq;
	uvm_analysis_port #(logic[15:0]) pwdata_port; //not sure if this works
	uvm_analysis_port #(spi_item_mosi) mosi_reg_value; // this packet contians CPOL, CPHA, LSBF and bit_en 
	
	spi_item_mosi itm;
	spi_item_mosi reg_itm;
	
	function new(string name="ref_model_wr_sb", uvm_component parent=null);
		super.new(name,parent);
			data_from_ip_mon_port = new("data_from_ip_mon_port", this);
			sclk_freq = new("sclk_freq",this);
			pwdata_port = new("pwdata_port", this);
			mosi_reg_value = new("mosi_reg_value",this);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
	endfunction
	
	logic[7:0] SCLK_clk_freq;
	int pclk, pclk_period, sclk, sclk_period;
	logic[15:0] pwdata = 0;
	logic[3:0] nbits;
	
	task run_phase(uvm_phase phase);
		forever begin
			data_from_ip_mon_port.get(itm);
			
			reg_itm = new();
			
			pclk_period = 2; //2ns
			pclk = 5*(10**8);
			sclk = pclk /itm.ccr.clk_DIV; //frequency
			sclk_period = 1/sclk;
			SCLK_clk_freq = pclk/sclk; //CCR
			
			sclk_freq.write(SCLK_clk_freq);
			nbits = itm.cr.BITS;
			pwdata = itm.pwdata;
			pwdata_port.write(pwdata);
			
			reg_itm.cr.CPOL = itm.cr.CPOL; 
			reg_itm.cr.CPHA = itm.cr.CPHA;
			reg_itm.cr.bit_en = itm.cr.bit_en;
			reg_itm.cr.LSBF = itm.cr.LSBF;
			reg_itm.pwrite = itm.pwrite;
			
			mosi_reg_value.write(reg_itm); // MODIFIED it was reg_itm.write(mosi_reg_value)
		end
	endtask

endclass
