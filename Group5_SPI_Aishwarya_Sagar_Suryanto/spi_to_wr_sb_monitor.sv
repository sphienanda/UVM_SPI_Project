class spi_to_wr_sb_monitor extends uvm_monitor;
	`uvm_component_utils(spi_to_wr_sb_monitor)

	uvm_tlm_analysis_fifo#(spi_item_mosi) item_from_ref_sb;
	
	uvm_analysis_port#(logic) mon_to_wr_sb_mosi_port;

	virtual spi_if intf;
	
	function new(string name="spi_to_wr_sb_monitor", uvm_component parent=null);
		super.new(name,parent);
		item_from_ref_sb = new("item_from_ref_sb",this);
		mon_to_wr_sb_mosi_port = new("mon_to_wr_sb_mosi_port", this);
	endfunction

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		if(!uvm_config_db#(virtual spi_if)::get(this,"*","intf", intf)) begin
			`uvm_fatal("SPI to WR SB MONITOR", "Failed in getting the input interface!!!")
		end
	endfunction

	spi_item_mosi itm;
	logic first_time_flag_mode_zero = 0;
	logic first_time_flag_mode_two = 0;
	logic CPOL;
	logic CPHA;
	logic mosi;
	logic ssel;
	reg [15:0] data;
	task run_phase(uvm_phase phase);
begin
`uvm_info("SPI-WR MONITOR", "Inside sclk monitor run phase", UVM_MEDIUM)
		forever begin	
			if(intf.ss == 1) begin
				first_time_flag_mode_zero = 0;
				first_time_flag_mode_two = 0;
			end	
		@(posedge intf.pclk) begin
			itm = new();
			 if(intf.ss == 0) begin
				item_from_ref_sb.get(itm);
				
				if((itm.cr.CPOL == 0) && (itm.cr.CPHA == 0))
				begin
					if(first_time_flag_mode_zero == 0) 
						begin
						#4;
						mosi = intf.mosi;
						mon_to_wr_sb_mosi_port.write(mosi);
						first_time_flag_mode_zero = 1;
						end
					else
						begin
							@(negedge intf.sclk)
							begin
							mosi = intf.mosi;
							mon_to_wr_sb_mosi_port.write(mosi);
							end
						end
				end
				else if((itm.cr.CPOL == 0) && (itm.cr.CPHA == 1))
				begin
					@(posedge intf.sclk) begin
						mosi = intf.mosi;
						mon_to_wr_sb_mosi_port.write(mosi);
					end
				end
				else if((itm.cr.CPOL == 1) && (itm.cr.CPHA == 0))
				begin
					if(first_time_flag_mode_two == 0) 
							begin
							#4;
							mosi = intf.mosi;
							mon_to_wr_sb_mosi_port.write(mosi);
							first_time_flag_mode_two = 1;
							end
						else
							begin
								@(posedge intf.sclk)
								begin
								mosi = intf.mosi;
								mon_to_wr_sb_mosi_port.write(mosi);
								end
							end
				end
				else if((itm.cr.CPOL == 1) && (itm.cr.CPHA == 1))
				begin
					@(negedge intf.sclk) begin
						mosi = intf.mosi;
						mon_to_wr_sb_mosi_port.write(mosi);
					end
				end		
			end
		end
		end
end
	endtask
endclass
