class spi_sclk_monitor extends uvm_monitor;
	`uvm_component_utils(spi_sclk_monitor)

	uvm_analysis_port#(int) sclk_smp;
	uvm_analysis_port#(logic) CPOL;
	virtual spi_if intf;

	function new(string name="spi_sclk_monitor", uvm_component parent=null);
		super.new(name,parent);
		sclk_smp = new("sclk_smp", this);
		CPOL = new("CPOL",this);
	endfunction

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		if(!uvm_config_db#(virtual spi_if)::get(this,"*","intf", intf)) begin
			`uvm_fatal("SCLK MONITOR", "Failed in getting the output interface!!!")
		end
		
	endfunction
	
	logic old_value;
	logic first_case_flag = 0;
	int counter = 0;
	
	task run_phase(uvm_phase phase);
		begin
			`uvm_info("SCLK MONITOR", "Inside sclk monitor run phase", UVM_MEDIUM)
		
		forever begin
			if(first_case_flag == 0) begin
				if(intf.sclk == 1'b0) begin
					CPOL.write(0);
				end
				if(intf.sclk == 1'b1) begin
					CPOL.write(1);
				end
			end

			@(posedge intf.pclk) begin
				if(intf.ss == 1) begin
					first_case_flag = 0;
					counter=0;
					old_value = intf.sclk;
				end
			
			if(intf.ss == 1'b0 && intf.penable == 1'b1) //if fails use while
				begin
					if(old_value != intf.sclk) begin
						if(first_case_flag == 0) begin
							$display("NEW TRANSACTION");
							counter-=1;
							sclk_smp.write(counter);
							first_case_flag = 1; 
							counter = 0;
						end
						else if(first_case_flag == 1) begin
							counter+=1;
							sclk_smp.write(counter);
							counter=0;
						end
					end
					else begin
						counter+=1;
					end
					old_value = intf.sclk;
				end
			end
		end
	//join_none
	$display("join_none");
	end
	endtask

endclass
