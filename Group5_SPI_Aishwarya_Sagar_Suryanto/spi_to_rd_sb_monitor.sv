class spi_to_rd_sb_monitor extends uvm_monitor;
	`uvm_component_utils(spi_to_rd_sb_monitor)

	uvm_analysis_port#(logic[31:0]) prdata_port;
	virtual spi_if intf;
	
	function new(string name="spi_to_rd_sb_monitor", uvm_component parent=null);
		super.new(name,parent);
		prdata_port = new("prdata_port",this);
	endfunction

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		if(!uvm_config_db#(virtual spi_if)::get(this,"*","intf", intf)) begin
			`uvm_fatal("SPI to WR SB MONITOR", "Failed in getting the input interface!!!")
		end
	endfunction

	logic [31:0] prdata;

	task run_phase(uvm_phase phase);
begin
`uvm_info("SPI-RD MONITOR", "Inside sclk monitor run phase", UVM_MEDIUM)
		fork
		forever begin
			@(posedge intf.pclk) begin
				if(intf.pwrite == 0) begin
					if(intf.penable == 1) begin
						if(intf.pready == 1) begin
							prdata = intf.prdata;
							prdata_port.write(prdata);
						end
					end
				end
			end
		end
	join_none
end
	endtask
endclass
