class spi_miso_driver extends uvm_driver #(spi_item_miso);
	`uvm_component_utils(spi_miso_driver)
	
	spi_item_miso reques;

	virtual spi_if intf;
	
	function new(string name = "spi_miso_driver", uvm_component parent = null);
		super.new(name, parent);
	endfunction : new
	
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		
		if (!uvm_config_db #(virtual spi_if)::get(this, "", "intf", intf))
			begin
			`uvm_error("FATAL MSG", "Object configuration failed");
			end
		$display("Driver - Build Phase");
	endfunction : build_phase
	
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
	endfunction : connect_phase
	
	logic [3:0] counter;
	
	task run_phase(uvm_phase phase);
		fork
			forever begin
				seq_item_port.get_next_item(reques);
				if(intf.ss == 0)
				begin
					if(reques.cr.bit_en == 0)
						begin	
							counter <= 8;
						end
					else if(reques.cr.bit_en == 1)
						begin
							counter <= reques.cr.BITS;
						end
						
						for(int i = 0; i < counter-1; i++) begin
							if((reques.cr.CPOL ^ reques.cr.CPHA) == 0)
								begin
									@(posedge intf.sclk);
									intf.miso <= reques.prdata[i];
								end
							else if((reques.cr.CPOL ^ reques.cr.CPHA) == 1)
								begin
									@(negedge intf.sclk);
									intf.miso <= reques.prdata[i];
								end
					
							else if(intf.ss == 1)
								break;
						end
				end
				seq_item_port.item_done();
			end
		join_none
	endtask : run_phase
	
endclass : spi_miso_driver

 
