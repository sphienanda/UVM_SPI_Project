class spi_env extends uvm_env;
	`uvm_component_utils(spi_env)
	
	spi_sclk_monitor sck_mon;
	spi_sck_sb sck_sb;
	ref_model_rd_mon rd_ref_mod_mon;
	ref_model_wr_mon wr_ref_mod_mon;
	spi_to_wr_sb_monitor wr_mon;
	spi_to_rd_sb_monitor rd_mon;
	ref_model_wr_sb wr_ref_mod_sb;
	ref_model_rd_sb rd_ref_mod_sb;
	spi_read_sb rd_sb;
	spi_write_sb wr_sb;
	
	spi_agent agt;
	
	function new(string name="spi_env", uvm_component parent=null);
		super.new(name,parent);
	endfunction

	function void build_phase(uvm_phase phase);
		sck_mon = new("sck_mon",this);
		sck_sb = new("sck_sb",this);
		rd_ref_mod_mon = new("rd_ref_mod_mon",this);
		wr_ref_mod_mon = new("wr_ref_mod_mon",this);
		wr_mon = new("wr_mon",this);
		rd_mon = new("rd_mon",this);
		wr_ref_mod_sb = new("wr_ref_mod_sb",this);
		rd_ref_mod_sb = new("rd_ref_mod_sb",this);
		rd_sb = new("rd_sb",this);
		wr_sb = new("wr_sb",this);
		agt = new("agt",this);
	endfunction
	
	function void connect_phase(uvm_phase phase);
		// Ref model connections
		wr_ref_mod_mon.wr_mon_port.connect(wr_ref_mod_sb.data_from_ip_mon_port.analysis_export);
		rd_ref_mod_mon.rd_mon_port.connect(rd_ref_mod_sb.data_from_ref_mod_mon_port.analysis_export);
		
		// SCK SB connections
		sck_mon.sclk_smp.connect(sck_sb.counter_from_sck_mon.analysis_export);
		sck_mon.CPOL.connect(sck_sb.CPOL_from_sck_mon.analysis_export);
		wr_ref_mod_sb.sclk_freq.connect(sck_sb.sck_freq_from_ref_mod.analysis_export);
		wr_ref_mod_sb.mosi_reg_value.connect(sck_sb.itm_from_ref_mod.analysis_export);
		
		// WR SB connections
		wr_ref_mod_sb.mosi_reg_value.connect(wr_mon.item_from_ref_sb.analysis_export);
		wr_mon.mon_to_wr_sb_mosi_port.connect(wr_sb.mosi_from_wr_mon_port);
		wr_ref_mod_sb.pwdata_port.connect(wr_sb.pwdata_from_ref_port.analysis_export);
		wr_ref_mod_sb.mosi_reg_value.connect(wr_sb.item_from_ref_port.analysis_export);
		
		// RD SB connections
		rd_mon.prdata_port.connect(rd_sb.data_port.analysis_export);
		rd_ref_mod_sb.prdata_port.connect(rd_sb.data_ref_port.analysis_export);
		
	endfunction

endclass
