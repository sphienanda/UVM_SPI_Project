`timescale 1ns/1ps

import uvm_pkg::*;



`include "spi_item_mosi.sv"
`include "spi_seq_mosi.sv"

`include "spi_item_miso.sv"
`include "spi_seq_miso.sv"

`include "spi_miso_driver.sv"
`include "spi_mosi_driver.sv"
`include "spi_mosi_sequencer.sv"
`include "spi_miso_sequencer.sv"
`include "ref_model_rd_monitor.sv"
`include "ref_model_wr_monitor.sv"
`include "sclk_monitor.sv"
`include "spi_to_wr_sb_monitor.sv"
`include "spi_to_rd_sb_monitor.sv"

`include "sck_scoreboard.sv"
`include "ref_model_wr_sb.sv"
`include "ref_model_rd_sb.sv"
`include "read_scoreboard.sv"
`include "write_scoreboard.sv"
`include "spi_agent.sv"
`include "spi_environment.sv"
`include "spi_test.sv"
`include "spi_if.sv"
`include "dut.sv"

import uvm_pkg::*;

module spi_top();
	reg clk, rst;
	
	spi_if intf(.pclk(clk), .prst_n(rst));
	
	spi U1 (intf.prst_n, intf.pclk, intf.psel, intf.pwrite, intf.pwdata, intf.penable, intf.prdata, intf.pready,  intf.paddr, intf.sclk, intf.miso, intf.mosi, intf.ss, intf.ccr, intf.cr, intf.sr);

	initial
		begin
			uvm_config_db #(virtual spi_if)::set(null,"*","intf",intf);
			run_test("spi_test");
		end

	initial begin
		rst = 0;
		#10;
		rst = 1;
		#265;

		rst = 0;
		#10;
		rst = 1;
		#265;

		rst = 0;
		#10;
		rst = 1;
		#265;

		rst = 0;
		#10;
		rst = 1;
		#265;

		rst = 0;
		#10;
		rst = 1;
		#265;
		rst = 0;
		#10;
		rst = 1;
		#265;
		rst = 0;
		#10;
		rst = 1;
		#265;
		rst = 0;
		#10;
		rst = 1;
		#265;

//--

		rst = 0;
		#10;
		rst = 1;
		#265;

		rst = 0;
		#10;
		rst = 1;
		#265;

		rst = 0;
		#10;
		rst = 1;
		#265;

		rst = 0;
		#10;
		rst = 1;
		#265;

		rst = 0;
		#10;
		rst = 1;
		#265;
		rst = 0;
		#10;
		rst = 1;
		#265;
		rst = 0;
		#10;
		rst = 1;
		#265;
		rst = 0;
		#10;
		rst = 1;
		#265;
		rst = 0;
		#10;
		rst = 1;
		#265;


		#100000;
		$finish;
	end
	always #1 clk = ~clk;

	initial begin
		clk = 0;
		
		end
	

	initial begin
	$dumpfile("spi.vcd");
	$dumpvars();
	end
endmodule
