class spi_write_sb extends uvm_scoreboard;
	`uvm_component_utils(spi_write_sb)
	
	uvm_analysis_imp#(logic,spi_write_sb) mosi_from_wr_mon_port;
	uvm_tlm_analysis_fifo#(spi_item_mosi) item_from_ref_port;
	uvm_tlm_analysis_fifo#(logic[15:0]) pwdata_from_ref_port;
		
	function new(string name="spi_write_sb", uvm_component parent=null);
		super.new(name,parent);
	endfunction
	
	function void build_phase(uvm_phase phase);
		mosi_from_wr_mon_port = new("mosi_from_wr_mon_port",this);
		pwdata_from_ref_port = new("pwdata_from_ref_port",this);
		item_from_ref_port = new("item_from_ref_port",this);
	endfunction
	
	spi_item_mosi itm;
	
	logic data_in[$];
	int counter;
	int cnt_check;
	
	logic [15:0] data;
	logic LSBF;
	logic [3:0] nbits;
	logic bit_en;
	logic [15:0] ref_data;
	
	reg set_counter_flag=0; 
	
	function void write(input logic mosi_bit);
		data_in.push_back(mosi_bit);
	endfunction
	
	task run_phase(uvm_phase phase);
		forever begin
			wait(data_in.size != 0) begin
				item_from_ref_port.get(itm);
				pwdata_from_ref_port.get(ref_data);
				
				LSBF = itm.cr.LSBF;
				bit_en = itm.cr.bit_en;
				nbits = itm.cr.BITS;
				
				if(set_counter_flag == 0) begin
					if(bit_en == 0)
					begin
						if(LSBF) begin
							counter = 0;
							cnt_check = 8;
						end
						else begin
							counter = 7;
							cnt_check = 0;
						end
					end
					else if(bit_en == 1)
					begin
						if(LSBF) begin
							counter = 0;
							if(nbits != 4'b0000)
								cnt_check = nbits;
							else if(nbits == 4'b0000)
								cnt_check = 16;
						end
						else begin
							if(nbits != 4'b0000)
								counter = nbits-1;
							else if(nbits == 4'b0000)
								counter = 15;
							cnt_check = 0;
						end
					end
					set_counter_flag = 1;
					data = 0;
				end
								
				if(LSBF == 1)
					begin
						data[counter] = data_in.pop_front();
						counter+=1;
						if(counter == cnt_check) begin
							if(bit_en == 0) begin
								if(data[7:0] == ref_data[7:0]) begin
									`uvm_info(get_type_name(), " Write function PASS!", UVM_MEDIUM); 
								end
								else begin
									$display("data: %b, ref_data: %b",data[7:0], ref_data[7:0]);
									`uvm_error(get_type_name(), " Write function FAIL!");
								end
							end else if(bit_en == 1) begin
								if(nbits != 4'b1000) begin
									if(data == ref_data) begin
										`uvm_info(get_type_name(), " Write function PASS!", UVM_MEDIUM); 
									end
									else begin
										$display("data: %b, ref_data: %b",data, ref_data);
										`uvm_error(get_type_name(), " Write function FAIL!");
									end
								end else if(nbits == 4'b1000) begin
									if(data[7:0] == ref_data[7:0]) begin
										`uvm_info(get_type_name(), " Write function PASS!", UVM_MEDIUM); 
									end
									else begin
										$display("data: %b, ref_data: %b",data[7:0], ref_data[7:0]);
										`uvm_error(get_type_name(), " Write function FAIL!");
									end
								end
							end
						end
					end
				else begin
					data[counter] = data_in.pop_front();
					counter-=1;
					if(counter < cnt_check) begin
						if(bit_en == 0) begin
							if(data[7:0] == ref_data[7:0]) begin
								`uvm_info(get_type_name(), " Write function PASS!", UVM_MEDIUM); 
							end
							else begin
								$display("data: %b, ref_data: %b",data[7:0], ref_data[7:0]);
								`uvm_error(get_type_name(), " Write function FAIL!");
							end
						end else if(bit_en == 1) begin
							if(nbits != 4'b1000) begin
								if(data == ref_data) begin
										`uvm_info(get_type_name(), " Write function PASS!", UVM_MEDIUM); 
								end
								else begin
									$display("data: %b, ref_data: %b",data, ref_data);
									`uvm_error(get_type_name(), " Write function FAIL!");
								end
							end
							else if(nbits == 4'b1000) begin
								if(data[7:0] == ref_data[7:0]) begin
										`uvm_info(get_type_name(), " Write function PASS!", UVM_MEDIUM); 
								end
								else begin
									$display("data: %b, ref_data: %b",data[7:0], ref_data[7:0]);
									`uvm_error(get_type_name(), " Write function FAIL!");
								end
							end
						end
					end
				end
			end
		end
	endtask
	
endclass
