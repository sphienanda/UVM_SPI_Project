class ref_model_rd_mon extends uvm_monitor;
	`uvm_component_utils(ref_model_rd_mon)
	
	uvm_analysis_port#(spi_item_miso) rd_mon_port;
	
	virtual spi_if intf;
	spi_item_miso itm;
	
	function new(string name="ref_model_rd_mon", uvm_component parent=null);
		super.new(name,parent);
		rd_mon_port = new("rd_mon_port",this);
	endfunction

	function void build_phase(uvm_phase phase);
		if(!uvm_config_db#(virtual spi_if)::get(this,"*","intf", intf)) begin
			`uvm_fatal("REF MODEL READ MONITOR", "Failed in getting the interface!!!")
		end
	endfunction

	task run_phase(uvm_phase phase);
		begin
		`uvm_info("REF-RD MONITOR", "Inside sclk monitor run phase", UVM_MEDIUM)
			fork
				forever begin
					@(posedge intf.pclk);
						if(intf.pwrite == 0) begin
							itm = new();
							itm.ccr.clk_DIV = intf.ccr.clk_DIV;
							itm.pwrite = intf.pwrite;
							itm.prdata = intf.prdata;
							itm.cr.CPOL = intf.cr.CPOL; //we need to define these in the interface because we need to pass this also to DUT
							itm.cr.CPHA = intf.cr.CPHA;
							itm.cr.bit_en = intf.cr.bit_en;
							itm.cr.LSBF = intf.cr.LSBF;
							itm.cr.BITS = intf.cr.BITS;
						rd_mon_port.write(itm);							
					end
				
				end
			join_none
		end
	endtask

endclass
