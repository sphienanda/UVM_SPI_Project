class spi_test extends uvm_test;
	`uvm_component_utils(spi_test)
	
	function new(string name="spi_test",uvm_component parent=null);
		super.new(name,parent);
	endfunction
	
	spi_env env;
	virtual spi_if intf;
	spi_seq_miso miso_seq;
	spi_seq_mosi mosi_seq;
	
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		env = spi_env::type_id::create("spi_env",this);
		mosi_seq = spi_seq_mosi::type_id::create("mosi sequence");
		miso_seq = spi_seq_miso::type_id::create("miso_sequence");
		if (!uvm_config_db#(virtual spi_if)::get(this, "", "intf", intf))
			`uvm_fatal("SPI TEST", "Interface cannot be fetched!!!!")
	endfunction
	
	function void end_of_elaboration_phase(uvm_phase phase);
		uvm_top.print_topology();
	endfunction
	
	task run_phase(uvm_phase phase);
		phase.raise_objection(this);
		miso_seq.start(env.agt.miso_seqr);
		mosi_seq.start(env.agt.mosi_seqr);
		phase.drop_objection(this);
	endtask
endclass
