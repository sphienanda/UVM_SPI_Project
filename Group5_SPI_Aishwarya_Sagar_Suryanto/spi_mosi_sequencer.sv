class spi_mosi_sequencer extends uvm_sequencer #(spi_item_mosi);
	`uvm_component_utils(spi_mosi_sequencer)
	
	function new(string name="spi_mosi_sequencer", uvm_component parent=null);
		super.new(name,parent);
	endfunction
	
endclass
