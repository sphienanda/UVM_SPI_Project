class spi_item_mosi extends uvm_sequence_item;
	`uvm_object_utils(spi_item_mosi)
	
	function new(string name="spi_item_mosi");
		super.new(name);
	endfunction

	typedef struct packed{
		logic bit_en;
		logic [1:0] reserved0;
		logic MSTR;
		logic LSBF;
		logic SPIE;
		logic [3:0] BITS;
		logic CPOL;
		logic CPHA;
		logic [15:12] reserved1;
	}spi_cr;
		
	//Status Reg for SPIF
	/*
	typedef struct packed {
		logic[7:1]  xyz;
		logic SPIF;
	}spi_ir;
	*/
	
	typedef struct packed{
		logic[7:0] clk_DIV;
	}spi_ccr;
	
	typedef struct packed{
		logic SPIF;
		logic WCOL;
		logic ROVR;
		logic MODF;
		logic ABRT;
		logic[2:0] rsvd;
	}spi_sr;
	
	typedef struct packed {
		logic[15:8] DH;
		logic[7:0] DL;
	}spi_dr;
	
				
	rand logic [31:0] paddr;	
	rand logic pwrite;		
	rand logic [31:0] pwdata;	
				

	//rand spi_cr spi_cr.bit_en;
	//rand logic spi_cr.LSBF;
	//rand logic spi_cr.BITS;
	rand spi_cr cr;
	rand spi_ccr ccr;

	

	function disp();
		`uvm_info("MOSI item", $sformatf("PWRITE = %b, PWDATA = %x, CPOL = %b, CPHA = %b, CLK DIV = %b, LSBF = %b, BITS = %d", pwrite, pwdata, cr.CPOL, cr.CPHA, ccr.clk_DIV,cr.LSBF, cr.BITS), UVM_MEDIUM) 
	endfunction: disp

endclass
