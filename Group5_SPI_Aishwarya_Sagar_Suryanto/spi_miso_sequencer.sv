class spi_miso_sequencer extends uvm_sequencer #(spi_item_miso);
	`uvm_component_utils(spi_miso_sequencer)
	
	function new(string name="spi_miso_sequencer", uvm_component parent=null);
		super.new(name,parent);
	endfunction
	
endclass
