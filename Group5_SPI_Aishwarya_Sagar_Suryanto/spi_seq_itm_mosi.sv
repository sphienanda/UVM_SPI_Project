class spi_item_mosi extends uvm_sequence_item;
	`uvm_object_utils(spi_item_mosi)
	

	typedef struct packed{
		logic bit_en;
		logic [1:0] reserved1;
		logic MSTR;
		logic LSBF;
		logic SPIE;
		logic [3:0] BITS;
		logic CPOL;
		logic CPHA;
		logic [15:12] reserved0;
	}spi_cr;
			
	typedef struct packed{
		logic[7:0] clk_DIV;
	}spi_ccr;
	
	typedef struct packed{
		logic SPIF;
		logic WCOL;
		logic ROVR;
		logic MODF;
		logic ABRT;
		logic[2:0] rsvd;
	}spi_sr;
	
	typedef struct packed {
		logic[15:8] DH;
		logic[7:0] DL;
	}spi_dr;
	
				// prdata;			>	APB
				// preset;			>	APB
				// pclk;			>	APB
				//
				// psel;		APB	>	SPI	
				// penable;		APB	>	SPI
	rand [31:0] paddr;	// paddr;		APB	>	SPI
	rand pwrite;		// pwrite;		APB	>	SPI
	rand [31:0] pwdata;	// pwdata;		APB	>	SPI
				// pready;		APB	<	SPI

	rand spi_cr.bit_en;
	rand spi_cr.LSBF;
	rand spi_cr.BITS;

	function new(string name="spi_item_mosi");
		super.new(name);
	endfunction

	function disp();
		`uvm_info("MOSI item::", $sformatf("PWRITE = %b, PWDATA = %x, CPOL = %b, CPHA = %b, CLK DIV = %b, LSBF = %b", pwrite, pwdata, spi_cr.CPOL, spi_cr.CPHA, spi_ccr.clk_DIV, spi_cr.LSBF), UVM_MEDIUM) 
	endfunction: disp

endclass
