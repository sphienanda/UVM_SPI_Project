class spi_item_miso extends uvm_sequence_item;
	`uvm_object_utils(spi_item_miso)
	

	typedef struct packed{
		logic bit_en;
		logic [1:0] reserved;
		logic MSTR;
		logic LSBF;
		logic SPIE;
		logic [3:0] BITS;
		logic CPOL;
		logic CPHA;
		logic [15:12] reserved;
	}spi_cr;
		
	//Status Reg for SPIF
	/*
	typedef struct packed {
		logic[7:1]  xyz;
		logic SPIF;
	}spi_ir;
	*/
	
	typedef struct packed{
		logic[7:0] clk_DIV;
	}spi_ccr;
	
	typedef struct packed{
		logic SPIF;
		logic WCOL;
		logic ROVR;
		logic MODF;
		logic ABRT;
		logic[2:0] rsvd;
	}spi_sr;
	
	typedef struct packed {
		logic[15:8] DH;
		logic[7:0] DL;
	}spi_dr;
	
				
	rand logic [7:0] paddr;	
	rand logic pwrite;		
	rand logic [7:0] prdata;	
				

	rand spi_cr cr;
	rand spi_ccr ccr;
	
	


	function new(string name="spi_item_miso");
		super.new(name);
	endfunction

	function disp();
		`uvm_info("MISO item:: ", $sformatf("PWRITE = %b, PRDATA = %x, CPOL = %b, CPHA = %b, CLK DIV = %b, LSBF = %b", pwrite, prdata, cr.CPOL, cr.CPHA, ccr.clk_DIV, cr.LSBF), UVM_MEDIUM)
	endfunction: disp

endclass
